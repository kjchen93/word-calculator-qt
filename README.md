# Word Calculator Qt
## Description
This is a Qt program originally written in C++, builds on `Qmake`. 
The program that receives a mathematical operation as a sentence,
performs the calculations and returns the result of the calculation.
Users will be prompted to give the mathematical operation in one sentence.


### Limitations
- Input numbers must be from 1 to 20 e.g. *one, two, three ... twenty*.
- Operators are limited to: *plus, minus, divide, times.*
- The structure of the input has to be in the following format:

    ```bash
    $ <number> <operator> <number>
    ```
- Spelling errors are not accounted for.
- User must input text in lower case only.


## Author

Kai Chen
