#include <QCoreApplication>

#include <iostream>
#include <iterator>
#include <algorithm>
#include <QTextStream>

/*
A program that receives a mathematical operation as a sentence,
performs the calculations and returns the result of the calculation.
*/


float calculate(int, char, int);
QVector<QString> wordparse(QString);

int main(int argc, char *argv[]){
    QCoreApplication a(argc, argv);
    QTextStream qin(stdin);
    QTextStream qout(stdout);
    float out;
    int num1, num2;
    char ch_op;
    QVector<QString> words;
    QMap<QString, int> number_dict;
    QMap<QString, char> operator_dict;

    // Map number in word form to integer
    QString word_nums[] = {"zero", "one", "two", "three", "four", "five",\
                               "six", "seven", "eight", "nine", "ten",\
                               "eleven", "twelve", "thirteen", "fourteen","fifteen",\
                               "sixteen", "seventeen", "eighteen", "nineteen", "twenty"};

    for(int i = 0; i < 21; i++){
        number_dict[word_nums[i]] = i;
    }

    // Map operators to respective chars
    operator_dict["times"] = '*';
    operator_dict["divide"] = '/';
    operator_dict["plus"] = '+';
    operator_dict["minus"] = '-';

    qout << "Give me an operation (e.g. three plus three) \n";
    qout.flush();

    QString input_string = qin.readLine();

    // parse the words
    words = wordparse(input_string);

    num1 = number_dict[words[0]];
    ch_op = operator_dict[words[1]];
    num2 = number_dict[words[2]];
    // perform calculation
    out = calculate(num1, ch_op, num2);
    // output results
    qout << "equals : \n" << out;
    qout.flush();

    return a.exec();

}

// Separate words from strings with space as delim
QVector<QString> wordparse(QString sent){
    QVector<QString> w{};

    w  = sent.split(" ");
    return w;

}

// Do operation
float calculate(int ia, char ch_op, int ib){
    float res = 0;

    switch (ch_op)
    {
    case '+':
        res = ia + ib;
        break;
    case '-':
        res = ia - ib;
        break;
    case '*':
        res = ia * ib;
        break;
    case '/':
        Q_ASSERT(ib != 0);
        res = (float)ia / (float)ib;
        break;
    }

    return res;
}
